rootProject.name = "entities"
if (file("api").exists()) includeBuild("api")
if (file("jpa").exists()) includeBuild("jpa")

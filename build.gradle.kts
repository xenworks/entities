plugins {
    `idea`
    id("com.brightsparklabs.gradle.multi-git").version("1.3.0")
}

group = "com.xenoterracide.entities"
version = "0.1.0-SNAPSHOT"

multiGitPluginConfig {
    repositoriesDir = file(".")
    repositories = mapOf(
            Pair("api", "git@bitbucket.org:xenworks/entity-api.git"),
            Pair("jpa", "git@bitbucket.org:xenworks/entity-jpa.git")
    )
}

tasks {
    "build" {
        if (gradle.includedBuilds.isEmpty()) {// TODO: dragons, what happens when we add a module
            dependsOn("gitClone")
            println("run build again to actually build!")
        } else {
            gradle.includedBuilds.forEach({ dependsOn(it.task(":build")) })
        }
    }
}
